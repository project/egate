
Payment Module  : EGATE
Author          : Josh Moore 
Settings        : admin > eC Settings > Receipt Types > eGate > Settings

********************************************************************
DESCRIPTION:

Accept payments using ANZ eGate using the Merchant Hosted Payment 
solution.

********************************************************************
SPECIAL NOTES

You must have curl compiled with PHP. You must also have an eGate
account. A strong cup of coffee is also recommended.


********************************************************************
CONFIGURATION:

admin > eC Settings > Receipt Types > eGate > Settings

Sett up your account ID and access code as given to you by eGate.


********************************************************************
TESTING:

You must set eGate to be running in test mode. Test mode simply
prepends 'TEST' to your account ID as specified by ANZ eGate.

admin > eC Settings > Receipt Types > eGate > Settings

The following shows test card numbers and associated expiry dates 
configured for each card scheme on the MIGS Payment server.

MasterCard 5123456789012346 05/13
MasterCard 5313581000123430 05/13
Visa 4005550000000001 05/13
Visa 4557012345678902 05/13
Amex 345678901234564 05/13
Bankcard (Australian Domestic) 5610901234567899 05/13
Diners Club 30123456789019 05/13

********************************************************************

See MAINTAINERS.txt for more maintenance info.
See README.txt (in e-Commerce root) for other info.